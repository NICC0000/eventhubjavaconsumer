FROM mcr.microsoft.com/java/jdk:8-zulu-alpine
ARG JAR_FILE=target/eventHubDataParsing-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} eventhubtest.jar
EXPOSE 80
ENTRYPOINT ["java","-jar","/eventhubtest.jar"]