package com.tcs.eventhub.util;


import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import lombok.extern.slf4j.Slf4j;

@Slf4j

public class DateUtility {

	private static final String DATE = "yyyy-MM-dd";
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final String DATE_24 = "HH:mm:ss";
    private static final String JSON_DATE ="yyyyMMdd";


	private DateUtility() {
	}
	
	public static Date stringToJsonDate(String dateInString) {

        SimpleDateFormat formatter = new SimpleDateFormat(JSON_DATE);
        Date date = null;
        try {
            date = formatter.parse(dateInString);
        } catch (ParseException e) {
            log.error(e.getMessage());
        }
        return date;
    }
	
	public static Date stringToDate(String dateInString) {

		SimpleDateFormat formatter = new SimpleDateFormat(DATE);
		Date date = null;
		try {

			date = formatter.parse(dateInString);

		} catch (ParseException e) {
			log.error(e.getMessage());
		}
		return date;
	}

	public static Date stringToDateNewFormat(String dateInString) throws ParseException {
		return new SimpleDateFormat(DATE).parse(dateInString);
	}
	
	public static String dateTimeToString(Date date) {

		return new SimpleDateFormat(DATE_FORMAT).format(date);
	}


	public static String dateToString(Date date) {

		return new SimpleDateFormat(DATE).format(date);
	}
	
	public static String dateToStringTwentyFour(Date date) {

		return new SimpleDateFormat(DATE_24).format(date);
	}
	
	public static Date stringToDateTwentyFour(String dateInString) {

		SimpleDateFormat formatter = new SimpleDateFormat(DATE_24);
		Date date = null;
		try {

			date = formatter.parse(dateInString);

		} catch (ParseException e) {
			log.error(e.getMessage());
		}
		return date;
	}

	public static Date stringToDateTime(String dateInString) {

		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		Date date = null;
		try {

			date = formatter.parse(dateInString);

		} catch (ParseException e) {

			log.error(e.getMessage());
		}
		return date;
	}

	public static Date loadingTimeFormatter(String date, String text, int min) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE);
		LocalTime t = LocalTime.parse(text).minusMinutes(min);
		Instant instant = t.atDate(LocalDate.parse(date, formatter)).atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(instant);

	}

	public static Date unloadingTimeFormatter(String date, String text, int min) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE);
		LocalTime t = LocalTime.parse(text).plusMinutes(min);
		Instant instant = t.atDate(LocalDate.parse(date, formatter)).atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(instant);

	}

	public static boolean isValidFormat(String format, String value) {

		Locale locale = new Locale("es", "ES");

		LocalDateTime ldt = null;
		DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(format, locale);

		try {
			ldt = LocalDateTime.parse(value, fomatter);
			String result = ldt.format(fomatter);
			return result.equals(value);
		} catch (DateTimeParseException e) {
			log.error(e.getMessage());
		}

		return false;
	}

	public static boolean isThisDateWithin12MonthsRange(String dateFromat) {

		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);
		try {
			
			Calendar currentDate = Calendar.getInstance();

			Calendar currentDateAfter12Months = Calendar.getInstance();

			currentDate.set(Calendar.HOUR_OF_DAY, 0);
			currentDate.set(Calendar.MINUTE, 0);
			currentDate.set(Calendar.SECOND, 0);
			currentDate.set(Calendar.MILLISECOND, 0);
			currentDateAfter12Months.set(Calendar.HOUR_OF_DAY, 0);
			currentDateAfter12Months.set(Calendar.MINUTE, 0);
			currentDateAfter12Months.set(Calendar.SECOND, 0);
			currentDateAfter12Months.set(Calendar.MILLISECOND, 0);
			currentDateAfter12Months.add(Calendar.YEAR, 1);

			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public static String timeZoneFormatter(Date date, String format, String zone) {

		SimpleDateFormat timeZoneFormatter = new SimpleDateFormat(format);
		TimeZone timeZone = TimeZone.getTimeZone(zone);
		timeZoneFormatter.setTimeZone(timeZone);
		return timeZoneFormatter.format(date);
	}
	
	public static LocalDate stringToLocalDate(String date) {
		 DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE);
		 
		  return LocalDate.parse(date, formatter);
	}
	
	public static Double roundTwoDecimals(Double d) {
		if(d != null) {
			DecimalFormat decimalFormatter = new DecimalFormat("#.##");
			decimalFormatter.setRoundingMode(RoundingMode.DOWN);
			try {
			return Double.parseDouble(decimalFormatter.format(d));
			}catch(Exception e) {
				log.info("EXCEPTION:"+d);
				return d;
			}
		}else {
			log.info("FOUND::"+d);
			return d;
		}
	}
	
	public static Month dateToMonth(String strDate) {
		LocalDate currentDate = LocalDate.parse(strDate); 
		return currentDate.getMonth(); 
    
	}
	
	public static boolean isThisDateWithin1MonthRange(String dateFromat, String dateToValidate,String startDate) {

		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);
		try {

			Date date = sdf.parse(dateToValidate);
			Calendar currentDate = Calendar.getInstance();
			
			Calendar currentDateAfter1Month = Calendar.getInstance();
			currentDateAfter1Month.setTime(DateUtility.stringToDate(startDate));
			
			currentDate.set(Calendar.HOUR_OF_DAY, 0);
			currentDate.set(Calendar.MINUTE, 0);
			currentDate.set(Calendar.SECOND, 0);
			currentDate.set(Calendar.MILLISECOND, 0);
			currentDateAfter1Month.set(Calendar.HOUR_OF_DAY, 0);
			currentDateAfter1Month.set(Calendar.MINUTE, 0);
			currentDateAfter1Month.set(Calendar.SECOND, 0);
			currentDateAfter1Month.set(Calendar.MILLISECOND, 0);
			currentDateAfter1Month.add(Calendar.DAY_OF_MONTH, 31);

			log.info("within 31 days"+date.before(currentDateAfter1Month.getTime())+"####"+date.after(currentDate.getTime()));
			 return (date.before(currentDateAfter1Month.getTime()) && date.after(currentDate.getTime()));

		} catch (ParseException e) {
			return false;
		}

	}
	
	public static LocalTime stringToLocalTime(String date) {
		   
        return LocalTime.parse(date.substring(0,2)+":"+date.substring(2));
    }
	
    public static LocalDate dateToLocalDate(Date date) {

    ZoneId defaultZoneId = ZoneId.systemDefault();
    Instant instant = date.toInstant();

    LocalDate localDate = instant.atZone(defaultZoneId).toLocalDate();
    log.info("Local Date is: "+localDate);
    return localDate;
}
}