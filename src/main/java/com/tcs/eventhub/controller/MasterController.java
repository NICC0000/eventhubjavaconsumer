package com.tcs.eventhub.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.tcs.eventhub.model.SaveResponse;
import com.tcs.eventhub.service.MasterService;

import lombok.extern.slf4j.Slf4j;

@RestController
@CrossOrigin
@Slf4j
public class MasterController {


	@Autowired
	MasterService masterService;
	
	@PostConstruct
	public void eventRegistration() throws Exception {
		masterService.transportJsonParsing();
		masterService.carrierVolume();
		masterService.carrierEventHubData();
		masterService.timeTableEventHubData();
	}
	
	@PostMapping("/sendVolumeFlow")
	public ResponseEntity<SaveResponse> SendVolumeFlow(@RequestBody JsonNode inputJson) throws Exception  {
		log.info("Inside SendVolumeFlow() controller");
		SaveResponse response = new SaveResponse();
        response.setMessage("VolumeFlow Data saved scuccessfully");
        masterService.volumeFlowMessage(inputJson);
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	@PostMapping("/sendCarrier")
	public ResponseEntity<SaveResponse> sendCarrier(@RequestBody JsonNode inputJson) throws Exception  {
		log.info("Inside sendCarrier() controller");
		SaveResponse response = new SaveResponse();
        response.setMessage("Carrier Data saved scuccessfully");
        masterService.carrier(inputJson);
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	@PostMapping("/sendLocation")
	public ResponseEntity<SaveResponse> sendLocation(@RequestBody JsonNode inputJson) throws Exception  {
		log.info("Inside SendLocation() controller");
		SaveResponse response = new SaveResponse();
        response.setMessage("Location Data saved scuccessfully");
        masterService.locationMessage(inputJson);
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	@PostMapping("/sendMatrix")
	public ResponseEntity<SaveResponse> sendMatrix(@RequestBody JsonNode inputJson) throws Exception  {
		log.info("Inside sendMatrix() controller");
		SaveResponse response = new SaveResponse();
        response.setMessage("Matrix Data saved scuccessfully");
        masterService.sendmatrix(inputJson);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PostMapping("/sendTransportTemplate")
	public ResponseEntity<SaveResponse> sendTransportTemplate(@RequestBody JsonNode inputJson) throws Exception  {
		log.info("Inside sendTransportTemplate() controller");
		SaveResponse response = new SaveResponse();
        response.setMessage("TransportTemplate Data saved scuccessfully");
        masterService.sendtransporttemplate(inputJson);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PostMapping("/sendGNSS")
	public ResponseEntity<SaveResponse> sendGnss(@RequestBody JsonNode inputJson) throws Exception  {
		log.info("Inside sendGNSS() controller");
		SaveResponse response = new SaveResponse();
        response.setMessage("GNSS Data saved scuccessfully");
        masterService.sendgnss(inputJson);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	@PostMapping("/sendTimeTableTemplate")
	public ResponseEntity<SaveResponse> sendTimeTableTemplate(@RequestBody JsonNode inputJson) throws Exception  {
		log.info("Inside sendTimeTableTemplate() controller");
		SaveResponse response = new SaveResponse();
        response.setMessage("TimeTableTemplate Data saved scuccessfully");
        masterService.timeTableTemplate(inputJson);
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	
	
	
}
