package com.tcs.eventhub.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="transport_template")
public class TransportTemplate {
	@Id
	@Column(name= "transport_id") 
	private String transportId;
	
	@Column(name= "transport_type") 
	private String transportType;
	
	@Column(name= "transport_number") 
	private String transportNumber;
	
	@Column(name= "transport_description") 
	private String transportDescription;

	@Column(name= "start_date") 
	private String startDate;
	
	@Column(name= "end_date") 
	private String endDate;
	
	@Column(name= "owner") 
	private String owner;
}
 