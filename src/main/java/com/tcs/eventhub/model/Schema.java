package com.tcs.eventhub.model;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Schema {

	private String type;
	
	private boolean optional;
	
	@JacksonXmlElementWrapper(useWrapping = false)
	List<Field> fields;

		
}
