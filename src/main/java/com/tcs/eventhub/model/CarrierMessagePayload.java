package com.tcs.eventhub.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CarrierMessagePayload {
	@JacksonXmlProperty(localName="ID")
	@JsonProperty("ID")
	private String id;
	
	@JacksonXmlProperty(localName="LB_ID")
	@JsonProperty("LB_ID")
	private String lbId;
	
	@JacksonXmlProperty(localName="LASTBTYP")
	@JsonProperty("LASTBTYP")
	private String lastbtyp;
	
	@JacksonXmlProperty(localName="REGNR")
	@JsonProperty("REGNR")
	private String regnr;
	
	@JacksonXmlProperty(localName="GPS_ENHET")
	@JsonProperty("GPS_ENHET")
	private String gpsenhet;
	
	@JacksonXmlProperty(localName="GPS_ENHET_INACTIVE")
	@JsonProperty("GPS_ENHET_INACTIVE")
	private String gpsEnhetInactive;
	
	@JacksonXmlProperty(localName="LAND")
	@JsonProperty("LAND")
	private String land;
	
	@JacksonXmlProperty(localName="HYRFORDON")
	@JsonProperty("HYRFORDON")
	private String hyrfordon;
	
	@JacksonXmlProperty(localName="TEMPSENSOR")
	@JsonProperty("TEMPSENSOR")
	private String tempsenor;
	
	@JacksonXmlProperty(localName="ARSMODELL")
	@JsonProperty("ARSMODELL")
	private String arsmodell;
	
	@JacksonXmlProperty(localName="LEVERANTOR")
	@JsonProperty("LEVERANTOR")
	private String leverantor;
	
	@JacksonXmlProperty(localName="BESIKTIGAD")
	@JsonProperty("BESIKTIGAD")
	private String besiktigad;
	
	@JacksonXmlProperty(localName="STATUS")
	@JsonProperty("STATUS")
	private String status;
	
	@JacksonXmlProperty(localName="SKADE_FORKLARING")
	@JsonProperty("SKADE_FORKLARING")
	private String skadeForklaring;
	
	@JacksonXmlProperty(localName="LOADABLE")
	@JsonProperty("LOADABLE")
	private String loadable;
	
	@JacksonXmlProperty(localName="P100_PALLET")
	@JsonProperty("P100_PALLET")
	private String p100Pallet;
	
	@JacksonXmlProperty(localName="P100_PARCEL")
	@JsonProperty("P100_PARCEL")
	private String p100Parcel;
	
	@JacksonXmlProperty(localName="ALLOWED_VOLUME")
	@JsonProperty("ALLOWED_VOLUME")
	private String allowedVolume;
	
	@JacksonXmlProperty(localName="VOLUME")
	@JsonProperty("VOLUME")
	private String volume;
}