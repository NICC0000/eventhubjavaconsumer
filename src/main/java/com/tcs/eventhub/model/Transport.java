package com.tcs.eventhub.model;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity

@Table(name="at_tls_se_transport")
public class Transport {

	@Id
	@Column(name = "transport_id")
	private String transportId;
	
	
    @Column(name = "ts")
	private BigInteger ts;
	

	@Column(name = "trasport_template")
	private String transportTemplate;
	
	@Column(name = "transport_type")
	private String transportType;
	
	@Column(name = "transport_no")
	private String transportNo;
	
	@Column(name = "transport_description")
	private String transportDescription;
	
	@Column(name = "prod_day")
	private String prodDay;
	
	@Column(name = "owner")
	private String owner;
	
	@Column(name = "haulage_contracter")
	private String haulageContracter;
	
	@Column(name = "set")
	private String set;
	
	@Column(name = "cause_of_extra_transport")
	private String causeOfExtraTransport;

}
