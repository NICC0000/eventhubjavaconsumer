package com.tcs.eventhub.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor



@Entity
@Table(name="timetable_template")

public class TimeTableTemplate {
	
	   
	
	@Id
	@Column(name= "timetable_id") 
	private String timetableId;
	
	
	@Column(name= "transport_id") 
	private String transportId;
	
	@Column(name= "journey_leg") 
	private String journeyLeg;
	
	@Column(name= "departure_location_id") 
	private String departureLocationId;
	
	@Column(name= "departure_location_name") 
	private String departureLocationName;
	
	@Column(name= "departure_start_date") 
	private String departureStartDate;
	
	@Column(name= "departure_end_date") 
	private String departureEndDate;
	
	@Column(name= "departure_time") 
	private String departureTime;
	
	@Column(name= "arrival_location_id") 
	private String arrivalLocationId;
	
	@Column(name= "arrival_location_name") 
	private String arrivalLocationName;
	
	@Column(name= "arrival_start_date") 
	private String arrivalStartDate;
	
	@Column(name= "arrival_end_date") 
	private String arrivalEndDate;
	
	@Column(name= "arrival_time") 
	private String arrivalTime;
	
	@Column(name= "transport_type") 
	private String transportType;

}
