package com.tcs.eventhub.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TransportTemplatePayload {
	@JacksonXmlProperty(localName="ID")
	@JsonProperty("ID")
	private int id;
	
	@JacksonXmlProperty(localName="TRANSPORTTYP")
	@JsonProperty("TRANSPORTTYP")
	private String transporttyp;
	
	@JacksonXmlProperty(localName="TRANSPORTNR")
	@JsonProperty("TRANSPORTNR")
	private int transportnr;
	
	@JacksonXmlProperty(localName="TRANSPORT_BESKRIVNING")
	@JsonProperty("TRANSPORT_BESKRIVNING")
	private String transportBeskrivning;

	@JacksonXmlProperty(localName="STARTDATUM")
	@JsonProperty("STARTDATUM")
	private String startdatum;
	
	@JacksonXmlProperty(localName="SLUTDATUM")
	@JsonProperty("SLUTDATUM")
	private String slutdatum;
	
	@JacksonXmlProperty(localName="AGARE")
	@JsonProperty("AGARE")
	private String agare;
}
