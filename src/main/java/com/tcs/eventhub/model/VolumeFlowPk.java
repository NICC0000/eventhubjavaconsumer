package com.tcs.eventhub.model;


import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;

import javax.persistence.Column;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VolumeFlowPk implements Serializable{
	
	private static final long serialVersionUID = 1L; 

	@Column(name = "origin_terminal_id")
    private String originTerminalId;
    
    @Column(name = "destination_terminal_id")
    private String destinationTerminalId;
    
    @Column(name = "date")
    private Date date;
    
    @Column(name = "time")
    private LocalTime time;
    

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((originTerminalId == null) ? 0 : originTerminalId.hashCode());
        result = prime * result + ((destinationTerminalId == null) ? 0 : destinationTerminalId.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((time == null) ? 0 : time.hashCode());
        
        return result;
    }
	
	 @Override
	    public boolean equals(Object obj) {
	        if (this == obj)
	            return true;
	        if (obj == null)
	            return false;
	        if (getClass() != obj.getClass())
	            return false;
	        
	        
	        VolumeFlowPk other = (VolumeFlowPk) obj;
	        
	        
	        if (originTerminalId == null) {
	            if (other.originTerminalId != null)
	                return false;
	        } else if (!originTerminalId.equals(other.originTerminalId)) {
	            return false;}
	        if (destinationTerminalId == null) {
	            if (other.destinationTerminalId != null)
	                return false;
	        } else if (!destinationTerminalId.equals(other.destinationTerminalId)) {
	            return false;
	        }
	            
	            if (date == null) {
		            if (other.date != null)
		                return false;
		        } else if (!date.equals(other.date)) {
		            return false;}
		        if (time == null) {
		            if (other.time != null)
		                return false;
		        } else if (!time.equals(other.time)) {
		            return false;   
                }
			return true;
	 }

}
