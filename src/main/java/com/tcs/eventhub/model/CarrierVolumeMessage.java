package com.tcs.eventhub.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@NoArgsConstructor
@JsonIgnoreProperties
@Getter
@Setter
public class CarrierVolumeMessage {
	 @JacksonXmlElementWrapper(useWrapping = false)
	   List<Schema> schema;
	   CarrierVolumePayload payload;
}
