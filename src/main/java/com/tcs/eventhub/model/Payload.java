package com.tcs.eventhub.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter

public class Payload {
	@JacksonXmlProperty(localName="TS")
	@JsonProperty("TS")
	private long ts;
	
	@JacksonXmlProperty(localName="ID")
	@JsonProperty("ID")
	private String id;
	
	@JacksonXmlProperty(localName="TRANSPORT_MALL")
	@JsonProperty("TRANSPORT_MALL")
	private String transportMall;
	
	@JacksonXmlProperty(localName="TRANSPORTTYP")
	@JsonProperty("TRANSPORTTYP")
	private String transportTyp;
	
	@JacksonXmlProperty(localName="TRANSPORTNR")
	@JsonProperty("TRANSPORTNR")
	private String transportNr;
	
	@JacksonXmlProperty(localName="TRANSPORT_BESKRIVNING")
	@JsonProperty("TRANSPORT_BESKRIVNING")
	private String transportBeskrivning;
	
	@JacksonXmlProperty(localName="PRODDYGN")
	@JsonProperty("PRODDYGN")
	private String prodDygn;
	
	@JacksonXmlProperty(localName="AGARE")
	@JsonProperty("AGARE")
	private String agare;
	
	@JacksonXmlProperty(localName="AKERI")
	@JsonProperty("AKERI")
	private String akeri;
	
	@JacksonXmlProperty(localName="INSTALLD")
	@JsonProperty("INSTALLD")
	private String installd;
	
	@JacksonXmlProperty(localName="ORSAK_EXTRATRANSPORT")
	@JsonProperty("ORSAK_EXTRATRANSPORT")
	private String orsakExtraTransport;
	
}
