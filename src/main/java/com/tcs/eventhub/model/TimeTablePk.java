package com.tcs.eventhub.model;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TimeTablePk implements Serializable {

	 private static final long serialVersionUID = 1L; 
     
     @Column(name = "ts")
     private BigInteger ts;
     
     
     @Column(name = "timetableId")
     private String timetableId;
     
     
     
     @Override
     public int hashCode() {
         final int prime = 31;
         int result = 1;
         result = prime * result + ((ts == null) ? 0 : ts.hashCode());
         result = prime * result + ((timetableId == null) ? 0 : timetableId.hashCode());
     return result;
     }



  



     @Override
     public boolean equals(Object obj) {
         if (this == obj)
             return true;
         if (obj == null)
             return false;
         if (getClass() != obj.getClass())
             return false;
         TimeTablePk other = (TimeTablePk) obj;
         if (ts == null) {
             if (other.ts != null)
                 return false;
         } else if (!ts.equals(other.ts)) {
             return false;}
         if (timetableId == null) {
             if (other.timetableId != null)
                 return false;
         } else if (!timetableId.equals(other.timetableId)) {
             return false;
         }
         
         return true;
         
     }
     
}
