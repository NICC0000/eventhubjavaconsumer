package com.tcs.eventhub.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@NoArgsConstructor
@Getter
@Setter
public class Schemas {
	
	@JacksonXmlElementWrapper(useWrapping = false)
	@JacksonXmlProperty(localName="AssignmentPartDriveToStart")
	@JsonProperty("AssignmentPartDriveToStart")
	@JsonInclude(Include.NON_NULL)
	private   List<Schema> sc;
}
