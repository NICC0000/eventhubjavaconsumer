package com.tcs.eventhub.model;

import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="gnss")
public class GnssData {

	@Id
	@Column(name= "iot_device_id") 
	private String iotDeviceID;
	
	
	@Column(name= "event_time") 
	//@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private String eventTime;

	@Column(name= "service_provider_server_time") 
	//@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private String serviceProviderServerTime;
	
	@Column(name= "event_type") 
	private String eventType;

	@Column(name= "service_provider_device_id") 
	private String serviceProviderDeviceID;
	
	@Column(name= "iot_server_time") 
	//@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private String iotServerTime;
	
	@Column(name= "longitude") 
	private String longitude;
	
	@Column(name= "latitude") 
	private String latitude;
	
	@Column(name= "speed") 
	private String speed;
	
	@Column(name= "heading") 
	private String heading;
	
	@Column(name= "odometer") 
	private String odometer;
	
	@Column(name= "ignition") 
	private String ignition;
	
	@Column(name= "service_provider") 
	private String serviceProvider;
	
	@Column(name= "domain") 
	private String domain;
	
	@Column(name= "gps_quality") 
	private String gpsQuality;
	
	@Column(name= "altitude") 
	private String altitude;
	
	@Column(name= "battery_voltage") 
	private String batteryVoltage;
	
	@Column(name= "temp_sensor") 
	private String tempSensor;
	   
	@Column(name= "door_sensor") 
	private String doorSensor;
	
	@Column(name= "shock_sensor") 
	private String shockSensor;
	
	@Column(name= "ebs_brake_lining_ok") 
	private String ebsBrakeLiningOk;
	
	@Column(name= "ebs_coupled") 
	private String ebsCoupled;
	
	@Column(name= "ebs_ignition") 
	private String ebsIgnition;
	
	@Column(name= "ebsL_load") 
	private String ebsLoad;
	
	@Column(name= "external_power") 
	private String externalPower;
	
	@Column(name= "last_vehicle_movement_timestamp") 
	//@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private String lastVehicleMovementTimestamp;
	   
	
  
}
