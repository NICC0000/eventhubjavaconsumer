package com.tcs.eventhub.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="at_tls_se_location")

public class Location {

	@Id
	@Column(name= "id") 
	private String id;
	
	@Column(name= "country") 
	private String country;
	
	@Column(name= "region") 
	private String region;
	
	@Column(name= "place_name") 
	private String placeName;
	
	@Column(name= "card") 
	private String card;
	
	@Column(name= "location_type") 
	private String locationType;
	
	@Column(name= "location_name") 
	private String locationName;
	
	@Column(name= "street") 
	private String street;
	
	@Column(name= "street_no") 
	private String streetNo;
	
	@Column(name= "street_no_prefix") 
	private String streetNoPrefix;
	
	@Column(name= "post_code") 
	private String postCode;
	 
	@Column(name= "lat") 
	private double lat;
	
	@Column(name= "lng") 
	private double lng;
	 
	     
}
