package com.tcs.eventhub.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="distance_matrix")
public class Matrix {

	@Id
	@Column(name= "origin_terminal_id") 
	private String originTerminalId;
	
	@Column(name= "origin_terminal_name") 
	private String originTerminalName;
	
	@Column(name= "destination_terminal_id") 
	private String destinationTerminalId;
	
	@Column(name= "destination_terminal_name") 
	private String destinationTerminalName;
	
	@Column(name= "distance") 
	private double distance;
	
	@Column(name= "hours") 
	private int hours;
	
	@Column(name= "minutes") 
	private int minutes;
	
	@Column(name= "speed") 
	private int speed;
}
