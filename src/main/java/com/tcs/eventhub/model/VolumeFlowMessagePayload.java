package com.tcs.eventhub.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties
@NoArgsConstructor
@Getter
@Setter
public class VolumeFlowMessagePayload {
	
	@JacksonXmlProperty(localName="COUNT")
	@JsonProperty("COUNT")
	private int count;
	
	@JacksonXmlProperty(localName="VOLUME")
	@JsonProperty("VOLUME")
	private double volume;
	
	@JacksonXmlProperty(localName="DATES")
	@JsonProperty("DATES")
	private String dates;
	
	@JacksonXmlProperty(localName="TIME")
	@JsonProperty("TIME")
	private String times;
	
	@JacksonXmlProperty(localName="ORIGIN")
	@JsonProperty("ORIGIN")
	private String origin;
	
	@JacksonXmlProperty(localName="DESTINATION")
	@JsonProperty("DESTINATION")
	private String destination;

	@JacksonXmlProperty(localName="WEIGHT")
	@JsonProperty("WEIGHT")
	private String weight;
	

	@JacksonXmlProperty(localName="TS")
	@JsonProperty("TS")
	private String ts;
	
	

}
