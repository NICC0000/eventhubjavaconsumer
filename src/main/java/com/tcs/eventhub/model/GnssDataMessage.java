package com.tcs.eventhub.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@JsonIgnoreProperties
@Getter
@Setter
public class GnssDataMessage {

	
	@JacksonXmlElementWrapper(useWrapping = false)
	   List<GnssSchema> schema;
	   GnssMessagePayload payload;
}
