package com.tcs.eventhub.model;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class GnssSchema {

	private int version;
	
	@JacksonXmlElementWrapper(useWrapping = false)
	List<Columns> columns;
	
}
