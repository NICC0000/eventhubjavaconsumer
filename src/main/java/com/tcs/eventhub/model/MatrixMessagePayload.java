package com.tcs.eventhub.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class MatrixMessagePayload {
	@JacksonXmlProperty(localName="START_P")
	@JsonProperty("START_P")
	private long startP;
	
	@JacksonXmlProperty(localName="START_NAME")
	@JsonProperty("START_NAME")
	private String startName;
	
	@JacksonXmlProperty(localName="STOP_P")
	@JsonProperty("STOP_P")
	private int stopP;
	
	@JacksonXmlProperty(localName="STOP_NAME")
	@JsonProperty("STOP_NAME")
	private String stopName;
	
	@JacksonXmlProperty(localName="KM")
	@JsonProperty("KM")
	private int km;
	
	@JacksonXmlProperty(localName="HOURS")
	@JsonProperty("HOURS")
	private int hours;
	
	@JacksonXmlProperty(localName="MINUTES")
	@JsonProperty("MINUTES")
	private int minutes;
	
	@JacksonXmlProperty(localName="SPEEDLIMIT")
	@JsonProperty("SPEEDLIMIT")
	private long speedLimit;
}
