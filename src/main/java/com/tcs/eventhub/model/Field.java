package com.tcs.eventhub.model;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Field {

	private String type;
	private boolean optional;
	private String name;
	private int version;
	private String field;
		
}
