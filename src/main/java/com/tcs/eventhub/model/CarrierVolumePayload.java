package com.tcs.eventhub.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CarrierVolumePayload {
	@JacksonXmlProperty(localName="TS")
	@JsonProperty("TS")
	private long ts;
	
	@JacksonXmlProperty(localName="VOLYMID")
	@JsonProperty("VOLYMID")
	private String volymId;
	
	@JacksonXmlProperty(localName="HIGH_LOAD")
	@JsonProperty("HIGH_LOAD")
	private String highLoad;
	
	@JacksonXmlProperty(localName="PALLET_VOLUME")
	@JsonProperty("PALLET_VOLUME")
	private String palletVolume;
	
	@JacksonXmlProperty(localName="ASTA_VOLUME")
	@JsonProperty("ASTA_VOLUME")
	private String astaVolume;
	
	@JacksonXmlProperty(localName="ASTA_COUNT")
	@JsonProperty("ASTA_COUNT")
	private String astaCount;
	
	@JacksonXmlProperty(localName="ASTA_WEIGHT")
	@JsonProperty("ASTA_WEIGHT")
	private String astaWeight;
	
	@JacksonXmlProperty(localName="ASTA_MIN_LOAD_DATETIME")
	@JsonProperty("ASTA_MIN_LOAD_DATETIME")
	private String AstaMinLoadDatetime;
	
	@JacksonXmlProperty(localName="ASTA_MAX_LOAD_DATETIME")
	@JsonProperty("ASTA_MAX_LOAD_DATETIME")
	private String astaMaxLoadDatetime;
	
	@JacksonXmlProperty(localName="PARCELS_LOOSELOAD")
	@JsonProperty("PARCELS_LOOSELOAD")
	private String parcelsLooseload;
	
	@JacksonXmlProperty(localName="PARCELS_CONTAINER")
	@JsonProperty("PARCELS_CONTAINER")
	private String parcelsContainer;
	
	@JacksonXmlProperty(localName="PARCELS_CARDBOARD")
	@JsonProperty("PARCELS_CARDBOARD")
	private String parcelsCardboard;
	
	@JacksonXmlProperty(localName="PARCELS_PALLET")
	@JsonProperty("PARCELS_PALLET")
	private String parcelsPallet;
	
	@JacksonXmlProperty(localName="PARCELS_LC")
	@JsonProperty("PARCELS_LC")
	private String parcelsLc;
	
	@JacksonXmlProperty(localName="PARCELS_LCAGE")
	@JsonProperty("PARCELS_LCAGE")
	private String parcelsLcage;
	
	@JacksonXmlProperty(localName="PARCELS_SCAGE")
	@JsonProperty("PARCELS_SCAGE")
	private String parcelsScage;
	
	@JacksonXmlProperty(localName="EMB_CONTAINER")
	@JsonProperty("EMB_CONTAINER")
	private String embContainer;
	
	@JacksonXmlProperty(localName="EMB_CARDBOARD")
	@JsonProperty("EMB_CARDBOARD")
	private String embCardboard;
	
	@JacksonXmlProperty(localName="EMB_PALLET")
	@JsonProperty("EMB_PALLET")
	private String embPallet;
	
	@JacksonXmlProperty(localName="EMB_LC")
	@JsonProperty("EMB_LC")
	private String embLc;
	
	@JacksonXmlProperty(localName="EMB_LCAGE")
	@JsonProperty("EMB_LCAGE")
	private String embLcage;
	
	@JacksonXmlProperty(localName="PALLETT_CONTAINER")
	@JsonProperty("PALLETT_CONTAINER")
	private String pallettContainer;
	
	@JacksonXmlProperty(localName="PALLETT_PALLET")
	@JsonProperty("PALLETT_PALLET")
	private String palletPallet;
	
	@JacksonXmlProperty(localName="EXPRESS_CONTAINER")
	@JsonProperty("EXPRESS_CONTAINER")
	private String expressContainer;
	
	@JacksonXmlProperty(localName="EXPRESS_PALLET")
	@JsonProperty("EXPRESS_PALLET")
	private String expressPallet;
	
	@JacksonXmlProperty(localName="DPD_CONTAINER")
	@JsonProperty("DPD_CONTAINER")
	private String dpdContainer;
	
	@JacksonXmlProperty(localName="DPD_CARDBOARD")
	@JsonProperty("DPD_CARDBOARD")
	private String dpdCardboard;
	
	@JacksonXmlProperty(localName="DPD_PALLET")
	@JsonProperty("DPD_PALLET")
	private String dpdPallet;
	
	@JacksonXmlProperty(localName="EMPTY_CONTAINER")
	@JsonProperty("EMPTY_CONTAINER")
	private String emptyContainer;
	
	@JacksonXmlProperty(localName="EMPTY_PALLET")
	@JsonProperty("EMPTY_PALLET")
	private String emptyPallet;
	
	@JacksonXmlProperty(localName="EMPTY_LC")
	@JsonProperty("EMPTY_LC")
	private String emptyLc;
	
	@JacksonXmlProperty(localName="EMPTY_LCAGE")
	@JsonProperty("EMPTY_LCAGE")
	private String emptyLcage;
	
	@JacksonXmlProperty(localName="EMPTY_SCAGE")
	@JsonProperty("EMPTY_SCAGE")
	private String emptyScage;
	
	@JacksonXmlProperty(localName="EMPTY_BC")
	@JsonProperty("EMPTY_BC")
	private String emptyBc;
	
	@JacksonXmlProperty(localName="INNIGHT_PALLET")
	@JsonProperty("INNIGHT_PALLET")
	private String innightPallet;
	
	@JacksonXmlProperty(localName="INNIGHT_LCAGE")
	@JsonProperty("INNIGHT_LCAGE")
	private String innightLcage;
	
	@JacksonXmlProperty(localName="INNIGHT_SCAGE")
	@JsonProperty("INNIGHT_SCAGE")
	private String innightScage;
	
	@JacksonXmlProperty(localName="LETTERA_PALLET")
	@JsonProperty("LETTERA_PALLET")
	private String letteraPallet;
	
	@JacksonXmlProperty(localName="LETTERA_LC")
	@JsonProperty("LETTERA_LC")
	private String letteraLc;
	
	@JacksonXmlProperty(localName="LETTERA_BC")
	@JsonProperty("LETTERA_BC")
	private String letteraBc;
	
	@JacksonXmlProperty(localName="LETTERB_PALLET")
	@JsonProperty("LETTERB_PALLET")
	private String letterbPallet;
	
	@JacksonXmlProperty(localName="LETTERB_LC")
	@JsonProperty("LETTERB_LC")
	private String letterbLc;
	
	@JacksonXmlProperty(localName="LETTERB_BC")
	@JsonProperty("LETTERB_BC")
	private String letterbBc;
	
	@JacksonXmlProperty(localName="FYLLNADSGRAD")
	@JsonProperty("FYLLNADSGRAD")
	private String fyllnadsgrad;
}