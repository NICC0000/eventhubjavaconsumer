package com.tcs.eventhub.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TimeTableTemplatePayload {
	
	@JacksonXmlProperty(localName="ID")
	@JsonProperty("ID")
	private String id;
	
	@JacksonXmlProperty(localName="TRANSPORT")
	@JsonProperty("TRANSPORT")
	private String transport;
	
	
	@JacksonXmlProperty(localName="DELSTRACKA")
	@JsonProperty("DELSTRACKA")
	private String delstracka;
	
	
	
	@JacksonXmlProperty(localName="AVG_PLATS")
	@JsonProperty("AVG_PLATS")
	private String avgPlats;
	
	@JacksonXmlProperty(localName="AVG_ANG")
	@JsonProperty("AVG_ANG")
	private String avgAng;
	
	@JacksonXmlProperty(localName="AVG_STARTDATUM")
	@JsonProperty("AVG_STARTDATUM")
	private String avgStartdatum;
	
	@JacksonXmlProperty(localName="AVG_SLUTDATUM")
	@JsonProperty("AVG_SLUTDATUM")
	private String avgSlutdatum;
	
	@JacksonXmlProperty(localName="AVG_TID")
	@JsonProperty("AVG_TID")
	private String avgTid;
	
	
	@JacksonXmlProperty(localName="ANK_PLATS")
	@JsonProperty("ANK_PLATS")
	private String ankPlats;
	
	@JacksonXmlProperty(localName="ANK_ANG")
	@JsonProperty("ANK_ANG")
	private String ankAng;
	
	@JacksonXmlProperty(localName="ANK_STARTDATUM")
	@JsonProperty("ANK_STARTDATUM")
	private String ankStartdatum;
	
	@JacksonXmlProperty(localName="ANK_SLUTDATUM")
	@JsonProperty("ANK_SLUTDATUM")
	private String ankSlutdatum;
	
	@JacksonXmlProperty(localName="ANK_TID")
	@JsonProperty("ANK_TID")
	private String ankTid;
	
	@JacksonXmlProperty(localName="TRANSPORTTYP")
	@JsonProperty("TRANSPORTTYP")
	private String transporttyp;
	
	
	
	

}
