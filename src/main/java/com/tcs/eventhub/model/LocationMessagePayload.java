package com.tcs.eventhub.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class LocationMessagePayload {
	@JacksonXmlProperty(localName="ID")
	@JsonProperty("ID")
	private int id;
	
	@JacksonXmlProperty(localName="LAND")
	@JsonProperty("LAND")
	private String Land;
	
	@JacksonXmlProperty(localName="PRODOMR")
	@JsonProperty("PRODOMR")
	private String prodomr;
	
	@JacksonXmlProperty(localName="ORTNAMN")
	@JsonProperty("ORTNAMN")
	private String ortnamn;
   
	@JacksonXmlProperty(localName="POSTORT")
	@JsonProperty("POSTORT")
	private String postort;
    
	@JacksonXmlProperty(localName="ANGTYP")
	@JsonProperty("ANGTYP")
	private String angtyp;
	
	@JacksonXmlProperty(localName="ANGORING")
	@JsonProperty("ANGORING")
	private String angoring;
	
	@JacksonXmlProperty(localName="GATA")
	@JsonProperty("GATA")
	private String gata;
	
	@JacksonXmlProperty(localName="GATA_NR")
	@JsonProperty("GATA_NR")
	private int gataNr;
	
	@JacksonXmlProperty(localName="GATA_NR_PREFIX")
	@JsonProperty("GATA_NR_PREFIX")
	private String gataNrPrefix;
	
	@JacksonXmlProperty(localName="POSTNR")
	@JsonProperty("POSTNR")
	private int postnr;
	
	@JacksonXmlProperty(localName="LAT")
	@JsonProperty("LAT")
	private double lat;
	
	@JacksonXmlProperty(localName="LNG")
	@JsonProperty("LNG")
	private double lng;
	
	
}
