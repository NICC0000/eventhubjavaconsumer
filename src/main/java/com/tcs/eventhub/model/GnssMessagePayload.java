package com.tcs.eventhub.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class GnssMessagePayload {

	@JacksonXmlProperty(localName="iotDeviceID")
	@JsonProperty("iotDeviceID")
	private String iotDeviceID;
	
	@JacksonXmlProperty(localName="eventTime")
	@JsonProperty("eventTime")
	private String eventTime;
	
	@JacksonXmlProperty(localName="serviceProviderServerTime")
	@JsonProperty("serviceProviderServerTime")
	private String serviceProviderServerTime;
	
	
	@JacksonXmlProperty(localName="eventType")
	@JsonProperty("eventType")
	private String eventType;
	
	@JacksonXmlProperty(localName="serviceProviderDeviceID")
	@JsonProperty("serviceProviderDeviceID")
	private String serviceProviderDeviceID;
	
	@JacksonXmlProperty(localName="iotServerTime")
	@JsonProperty("iotServerTime")
	private String iotServerTime;
	
	@JacksonXmlProperty(localName="longitude")
	@JsonProperty("longitude")
	private double longitude;
	
	@JacksonXmlProperty(localName="latitude")
	@JsonProperty("latitude")
	private double latitude;
	
	@JacksonXmlProperty(localName="speed")
	@JsonProperty("speed")
	private int speed;
	
	@JacksonXmlProperty(localName="heading")
	@JsonProperty("heading")
	private int heading;
	
	@JacksonXmlProperty(localName="odometer")
	@JsonProperty("odometer")
	private int odometer;
	
	@JacksonXmlProperty(localName="ignition")
	@JsonProperty("ignition")
	private Boolean ignition;
	

	@JacksonXmlProperty(localName="serviceProvider")
	@JsonProperty("serviceProvider")
	private String serviceProvider;
	
	@JacksonXmlProperty(localName="domain")
	@JsonProperty("domain")
	private String domain;
	
	@JacksonXmlProperty(localName="GPSQuality")
	@JsonProperty("GPSQuality")
	private String GPSQuality;
	
	@JacksonXmlProperty(localName="altitude")
	@JsonProperty("altitude")
	private String altitude;
	
	@JacksonXmlProperty(localName="batteryVoltage")
	@JsonProperty("batteryVoltage")
	private String batteryVoltage;
	
	@JacksonXmlProperty(localName="tempSensor")
	@JsonProperty("tempSensor")
	private String tempSensor;
	
	@JacksonXmlProperty(localName="doorSensor")
	@JsonProperty("doorSensor")
	private String doorSensor;
	
	@JacksonXmlProperty(localName="shockSensor")
	@JsonProperty("shockSensor")
	private String shockSensor;
	
	@JacksonXmlProperty(localName="ebsBrakeLining_ok")
	@JsonProperty("ebsBrakeLining_ok")
	private String ebsBrakeLining_ok;
	
	@JacksonXmlProperty(localName="ebsCoupled")
	@JsonProperty("ebsCoupled")
	private String ebsCoupled;
	
	@JacksonXmlProperty(localName="ebsIgnition")
	@JsonProperty("ebsIgnition")
	private String ebsIgnition;
	
	@JacksonXmlProperty(localName="ebsLoad")
	@JsonProperty("ebsLoad")
	private String ebsLoad;
	
	@JacksonXmlProperty(localName="externalPower")
	@JsonProperty("externalPower")
	private String externalPower;
	
	@JacksonXmlProperty(localName="lastVehicleMovementTimestamp")
	@JsonProperty("lastVehicleMovementTimestamp")
	private String lastVehicleMovementTimestamp;
	
}


