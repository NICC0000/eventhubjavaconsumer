package com.tcs.eventhub.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="tls_se_carrier")
public class Carrier {

	@Id
	@Column(name= "carrier_id") 
	private String carrierId;
	
	
	@Column(name= "asset_id") 
	private String assetId;
	
	@Column(name= "asset_type") 
	private String assetType;
	
	@Column(name= "asset_regno") 
	private String assetRegno;
	
	@Column(name= "gps_unit") 
	private String gpsUnit;
	
	@Column(name= "gps_unit_inactive") 
	private String gpsUnitInactive;
	
	@Column(name= "country") 
	private String country;
	
	@Column(name= "hire_vehicle") 
	private String hireVehicle;
	
	@Column(name= "temp_sensor") 
	private String tempSensor;
	
	@Column(name= "year") 
	private String year;
	
	@Column(name= "supplier") 
	private String supplier;
	
	@Column(name= "last_inspection") 
	private String lastInspection;
	
	@Column(name= "status") 
	private String status;
	
	@Column(name= "damage_explanation") 
	private String damageExplanation;
	
	@Column(name= "loadable") 
	private String loadable;
	
	@Column(name= "p100_pallets") 
	private String p100Pallets;
	
	@Column(name= "p100_parcels") 
	private String p100Parcels;
	
	@Column(name= "allowed_volume") 
	private String allowedVolume;
	
	@Column(name= "volume") 
	private String volume;
}