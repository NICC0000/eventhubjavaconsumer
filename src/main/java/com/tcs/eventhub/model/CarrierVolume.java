package com.tcs.eventhub.model;

import java.math.BigInteger;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="tls_se_carrier_volume")

public class CarrierVolume {

	@Id
	@Column(name = "volume_id")
	private String volumeId;
	
	 @Column(name = "ts")
	private BigInteger ts;
	
	@Column(name = "high_load")
	private String highLoad;
	
	@Column(name = "pallet_volume")
	private String palletVolume;
	
	@Column(name = "asta_volume")
	private String astaVolume;
	
	@Column(name = "asta_count")
	private String astaCount;
	
	@Column(name = "asta_weight")
	private String astaWeight;
	
	
	
	@Column(name = "asta_min_load_datetime")
	private String astaMinLoadDatetime;
	
	@Column(name = "asta_max_load_datetime")
	private String astaMaxLoadDatetime;
	
	@Column(name = "parcels_looseload")
	private String parcelsLooseload;
	
	@Column(name = "parcels_container")
	private String parcelsContainer;
	
	@Column(name = "parcels_cardboard")
	private String parcelsCardboard;
	
	@Column(name = "parcels_pallet")
	private String parcelsPallet;
	
	@Column(name = "parcels_lc")
	private String parcelsLc;
	
	@Column(name = "parcels_lcage")
	private String parcelsLcage;
	
	@Column(name = "parcels_scage")
	private String parcelsScage;
	
	@Column(name = "emb_container")
	private String embContainer;
	
	
	@Column(name = "emb_cardboard")
	private String embCardboard;
	
	@Column(name = "emb_pallet")
	private String embPallet;
	
	@Column(name = "emb_lc")
	private String embLc;
	
	@Column(name = "emb_lcage")
	private String embLcage;
	
	@Column(name = "pallett_container")
	private String pallettContainer;
	
	@Column(name = "pallett_pallett")
	private String pallettPallett;
	
	@Column(name = "express_container")
	private String expressContainer;
	
	@Column(name = "express_pallet")
	private String expressPallet;
	
	@Column(name = "dpd_container")
	private String dpdContainer;
	
	@Column(name = "dpd_cardboard")
	private String dpdCardboard;
	
	@Column(name = "dpd_pallet")
	private String dpdPallet;
	
	@Column(name = "empty_container")
	private String emptyContainer;
	
	@Column(name = "empty_pallet")
	private String emptyPallet;
	
	@Column(name = "empty_lc")
	private String emptyLc;
	
	@Column(name = "empty_lcage")
	private String emptyLcage;
	
	@Column(name = "empty_scage")
	private String emptyScage;
	
	@Column(name = "empty_bc")
	private String emptyBc;
	
	@Column(name = "innight_pallet")
	private String innightPallet;
	
	@Column(name = "innight_lcage")
	private String innightLcage;
	
	@Column(name = "innight_scage")
	private String innightScage;
	
	@Column(name = "lettera_pallet")
	private String letteraPallet;
	
	@Column(name = "lettera_lc")
	private String letteraLc;
	
	@Column(name = "lettera_bc")
	private String letteraBc;
	
	@Column(name = "letterb_pallet")
	private String letterbPallet;
	
	@Column(name = "letterb_lc")
	private String letterbLc;
	
	@Column(name = "letterb_bc")
	private String letterbBc;
	
	@Column(name = "fillratio")
	private String fillratio;
}
