package com.tcs.eventhub.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor



@Entity
@Table(name="kafka_transaction_raw_30min_data")
public class VolumeFlow {
	@EmbeddedId
	 VolumeFlowPk volumeFlowPk;
	
	 
	    @Column(name = "no_of_parcel_quantity")
	    private int noOfParcelQuantity;
	    
	    @Column(name = "parcel_volume")
	    private double parcelVolume;
	    
	    @Column(name = "created_by")
	    private String createdBy;
	    
	    @Column(name = "created_ts")
	    private Date createdTs;
	    
	    @Column(name = "updated_by")
	    private String updatedBy;
	    
	    @Column(name = "updated_ts")
	    private Date updatedTs;
}
