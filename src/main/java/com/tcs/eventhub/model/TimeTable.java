package com.tcs.eventhub.model;


import java.math.BigInteger;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor



@Entity
@Table(name="at_tls_se_timetable")
public class TimeTable {
        @Id
	    @Column(name = "timetableId")
	    private String timetableId;
	
	    @Column(name = "ts")
        private BigInteger ts;
     
	    
	    @Column(name = "transport")
	    private String transport;
	    
	    @Column(name = "journey_leg")
	    private String journeyLeg;
	    
	    @Column(name = "departure_location_id")
	    private String departureLocationId;
	    
	    @Column(name = "departure_location_name")
	    private String departureLocationName;
	    
	    @Column(name = "departure_date")
	    private String departureDate;
	    
	    @Column(name = "departure_time")
	    private String departureTime;
	    
	    @Column(name = "actual_departure_time")
	    private String actualDepartureTime;
	    
	    @Column(name = "departure_loaction")
	    private String departureLocation;
	    
	    @Column(name = "arrival_location_id")
	    private String arrivalLocationId;
	    
	    @Column(name = "arrival_location_name")
	    private String arrivalLocationName;
	    
	    @Column(name = "arrival_date")
	    private String arrivalDate;
	    
	    @Column(name = "arrival_time")
	    private String arrivalTime;
	    
	    @Column(name = "actual_arrival_time")
	    private String actualArrivalTime;
	    
	    @Column(name = "arrival_location")
	    private String arrivalLocation;
	    
	    @Column(name = "distance")
	    private String distance;
	    
	    @Column(name = "departure_set")
	    private String departureSet;
	    
	    @Column(name = "arrival_set")
	    private String arrivalSet;
	    
	    @Column(name = "assert_type_id1")
	    private String assertTypeId1;
	    
	    @Column(name = "assert_type1")
	    private String assertType1;
	    
	    @Column(name = "asset_id1")
	    private String assetId1;
	    
	    @Column(name = "asset_regno1")
	    private String assetRegno1;
	    
	    @Column(name = "capacity1")
	    private String capacity1;
	    
	    @Column(name = "utilization_rate1")
	    private String utilizationRate1;
	    
	    @Column(name = "asset_type_id2")
	    private String assetTypeId2;
	    
	    @Column(name = "asset_type2")
	    private String assetType2;
	    
	    @Column(name = "asset_id2")
	    private String assetId2;
	    
	    @Column(name = "asset_regno2")
	    private String assetRegno2;
	    
	    @Column(name = "capacity2")
	    private String capacity2;
	    
	    @Column(name = "utilization_rate2")
	    private String utilizationRate2;
	    
	    @Column(name = "asset_type_id3")
	    private String assetTypeId3;
	    
	    @Column(name = "asset_type3")
	    private String assetType3;
	    
	    @Column(name = "asset_id3")
	    private String assetId3;
	    
	    @Column(name = "asset_regno3")
	    private String assetRegno3;
	    
	    @Column(name = "capacity3")
	    private String capacity3;
	    
	    @Column(name = "utilization_rate3")
	    private String utilizationRate3;
	    
	    @Column(name = "asset_type_id4")
	    private String assetTypeId4;
	    
	    @Column(name = "asset_type4")
	    private String assetType4;
	    
	    @Column(name = "asset_id4")
	    private String assetId4;
	    
	    @Column(name = "asset_regno4")
	    private String assetRegno4;
	    
	    @Column(name = "capacity4")
	    private String capacity4;
	    
	    @Column(name = "utilization_rate4")
	    private String utilizationRate4;
}
