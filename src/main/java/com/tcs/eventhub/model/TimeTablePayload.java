package com.tcs.eventhub.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TimeTablePayload {

	@JacksonXmlProperty(localName="TS")
	@JsonProperty("TS")
	private long ts;
	
	@JacksonXmlProperty(localName="ID")
	@JsonProperty("ID")
	private String id;
	
	@JacksonXmlProperty(localName="TRANSPORT")
	@JsonProperty("TRANSPORT")
	private String transport;
	
	@JacksonXmlProperty(localName="DELSTRACKA")
	@JsonProperty("DELSTRACKA")
	private String delstracka;
	
	@JacksonXmlProperty(localName="AVG_PLATS")
	@JsonProperty("AVG_PLATS")
	private String avgPlats;
	
	@JacksonXmlProperty(localName="AVG_ANG")
	@JsonProperty("AVG_ANG")
	private String avgAng;
	
	@JacksonXmlProperty(localName="AVG_DATUM")
	@JsonProperty("AVG_DATUM")
	private String avgDatum;
	
	@JacksonXmlProperty(localName="AVG_TID")
	@JsonProperty("AVG_TID")
	private String avgTid;
	
	@JacksonXmlProperty(localName="VERKL_AVG_TID")
	@JsonProperty("VERKL_AVG_TID")
	private String verklAvgTid;
	
	@JacksonXmlProperty(localName="AVG_ANM")
	@JsonProperty("AVG_ANM")
	private String avgAnm;
	
	@JacksonXmlProperty(localName="ANK_PLATS")
	@JsonProperty("ANK_PLATS")
	private String ankPlats;
	
	@JacksonXmlProperty(localName="ANK_ANG")
	@JsonProperty("ANK_ANG")
	private String ankAng;
	
	@JacksonXmlProperty(localName="ANK_DATUM")
	@JsonProperty("ANK_DATUM")
	private String ankDatum;
	
	@JacksonXmlProperty(localName="ANK_TID")
	@JsonProperty("ANK_TID")
	private String ankTid;
	
	@JacksonXmlProperty(localName="VERKL_ANK_TID")
	@JsonProperty("VERKL_ANK_TID")
	private String verklAnkTid;
	
	@JacksonXmlProperty(localName="ANK_ANM")
	@JsonProperty("ANK_ANM")
	private String ankAnm;
	
	@JacksonXmlProperty(localName="STRACKA")
	@JsonProperty("STRACKA")
	private String stracka;
	
	@JacksonXmlProperty(localName="AVG_INSTALLD")
	@JsonProperty("AVG_INSTALLD")
	private String avgInstalld;
	
	@JacksonXmlProperty(localName="ANK_INSTALLD")
	@JsonProperty("ANK_INSTALLD")
	private String ankInstalld;
	
	@JacksonXmlProperty(localName="LASTB_DEF_1")
	@JsonProperty("LASTB_DEF_1")
	private String lastbDef1;
	
	@JacksonXmlProperty(localName="LASTB_1")
	@JsonProperty("LASTB_1")
	private String lastb1;
	
	@JacksonXmlProperty(localName="LBID_1")
	@JsonProperty("LBID_1")
	private String lbid1;
	
	@JacksonXmlProperty(localName="REGNR_1")
	@JsonProperty("REGNR_1")
	private String regnr1;
	
	@JacksonXmlProperty(localName="VOLYMID_1")
	@JsonProperty("VOLYMID_1")
	private String volymid1;
	
	@JacksonXmlProperty(localName="FYLLNADSGRAD_1")
	@JsonProperty("FYLLNADSGRAD_1")
	private String fyllnadsgrad1;
	
	@JacksonXmlProperty(localName="LASTB_DEF_2")
	@JsonProperty("LASTB_DEF_2")
	private String lastbDef2;
	
	@JacksonXmlProperty(localName="LASTB_2")
	@JsonProperty("LASTB_2")
	private String lastb2;
	
	@JacksonXmlProperty(localName="LBID_2")
	@JsonProperty("LBID_2")
	private String lbid2;
	
	@JacksonXmlProperty(localName="REGNR_2")
	@JsonProperty("REGNR_2")
	private String regnr2;
	
	@JacksonXmlProperty(localName="VOLYMID_2")
	@JsonProperty("VOLYMID_2")
	private String volymid2;
	
	@JacksonXmlProperty(localName="FYLLNADSGRAD_2")
	@JsonProperty("FYLLNADSGRAD_2")
	private String fyllnadsgrad2;
	
	@JacksonXmlProperty(localName="LASTB_DEF_3")
	@JsonProperty("LASTB_DEF_3")
	private String lastbDef3;
	
	@JacksonXmlProperty(localName="LASTB_3")
	@JsonProperty("LASTB_3")
	private String lastb3;
	
	@JacksonXmlProperty(localName="LBID_3")
	@JsonProperty("LBID_3")
	private String lbid3;
	
	@JacksonXmlProperty(localName="REGNR_3")
	@JsonProperty("REGNR_3")
	private String regnr3;
	
	@JacksonXmlProperty(localName="VOLYMID_3")
	@JsonProperty("VOLYMID_3")
	private String volymid3;
	
	@JacksonXmlProperty(localName="FYLLNADSGRAD_3")
	@JsonProperty("FYLLNADSGRAD_3")
	private String fyllnadsgrad3;
	
	@JacksonXmlProperty(localName="LASTB_DEF_4")
	@JsonProperty("LASTB_DEF_4")
	private String lastbDef4;
	
	@JacksonXmlProperty(localName="LASTB_4")
	@JsonProperty("LASTB_4")
	private String lastb4;
	
	@JacksonXmlProperty(localName="LBID_4")
	@JsonProperty("LBID_4")
	private String lbid4;
	
	@JacksonXmlProperty(localName="REGNR_4")
	@JsonProperty("REGNR_4")
	private String regnr4;
	
	@JacksonXmlProperty(localName="VOLYMID_4")
	@JsonProperty("VOLYMID_4")
	private String volymid4;
	
	@JacksonXmlProperty(localName="FYLLNADSGRAD_4")
	@JsonProperty("FYLLNADSGRAD_4")
	private String fyllnadsgrad4;
}