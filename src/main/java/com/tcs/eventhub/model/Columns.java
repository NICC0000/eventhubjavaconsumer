package com.tcs.eventhub.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Columns {
 
	private String Type;
	private String Field;
	private String Unit;
	
	
}
