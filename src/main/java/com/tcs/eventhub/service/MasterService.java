package com.tcs.eventhub.service;

import java.math.BigInteger;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.Message;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.azure.spring.integration.core.AzureHeaders;
import com.azure.spring.integration.core.api.CheckpointConfig;
import com.azure.spring.integration.core.api.CheckpointMode;
import com.azure.spring.integration.core.api.StartPosition;
import com.azure.spring.integration.core.api.reactor.Checkpointer;
import com.azure.spring.integration.eventhub.api.EventHubOperation;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.tcs.eventhub.model.Carrier;
import com.tcs.eventhub.model.CarrierMessage;
import com.tcs.eventhub.model.CarrierMessagePayload;
import com.tcs.eventhub.model.CarrierVolume;
import com.tcs.eventhub.model.CarrierVolumeMessage;
import com.tcs.eventhub.model.CarrierVolumePayload;
import com.tcs.eventhub.model.GnssData;
import com.tcs.eventhub.model.GnssDataMessage;
import com.tcs.eventhub.model.GnssMessagePayload;
import com.tcs.eventhub.model.Location;
import com.tcs.eventhub.model.LocationMessage;
import com.tcs.eventhub.model.LocationMessagePayload;
import com.tcs.eventhub.model.Matrix;
import com.tcs.eventhub.model.MatrixMessage;
import com.tcs.eventhub.model.MatrixMessagePayload;
import com.tcs.eventhub.model.Payload;
import com.tcs.eventhub.model.TimeTable;
import com.tcs.eventhub.model.TimeTableMessage;
import com.tcs.eventhub.model.TimeTablePayload;
import com.tcs.eventhub.model.TimeTablePk;
import com.tcs.eventhub.model.TimeTableTemplate;
import com.tcs.eventhub.model.TimeTableTemplateMessage;
import com.tcs.eventhub.model.TimeTableTemplatePayload;
import com.tcs.eventhub.model.Transport;
import com.tcs.eventhub.model.TransportMessage;
import com.tcs.eventhub.model.TransportTemplate;
import com.tcs.eventhub.model.TransportTemplateMessage;
import com.tcs.eventhub.model.TransportTemplatePayload;
import com.tcs.eventhub.model.VolumeFlow;
import com.tcs.eventhub.model.VolumeFlowMessage;
import com.tcs.eventhub.model.VolumeFlowMessagePayload;
import com.tcs.eventhub.model.VolumeFlowPk;
import com.tcs.eventhub.repositories.CarrierRepository;
import com.tcs.eventhub.repositories.CarrierVolumeRepository;
import com.tcs.eventhub.repositories.GnssRepository;
import com.tcs.eventhub.repositories.LocationRepository;
import com.tcs.eventhub.repositories.MatrixRepository;
import com.tcs.eventhub.repositories.TimeTableRepository;
import com.tcs.eventhub.repositories.TimeTableTemplateRepository;
import com.tcs.eventhub.repositories.TransportRepository;
import com.tcs.eventhub.repositories.TransportTemplateRepository;
import com.tcs.eventhub.repositories.VolumeFlowRepository;
import com.tcs.eventhub.util.DateUtility;



@Service
public class MasterService {

	@Value("${transportEventHubName}")
	String transportEventHubName;
	
	@Value("${carrierVolumeEventHubName}")
	String carrierVolumeEventHubName;
	
	@Value("${carrierEventHubName}")
	String carrierEventHubName;
	
	@Value("${timeTableEventHubName}")
	String timeTableEventHubName;
	
	@Value("${consumerName}")
	String consumerName;
	
	@Autowired
	EventHubOperation eventHubOperation;
	
	
	@Autowired
	TransportRepository transportRepository;
	
	@Autowired
	CarrierVolumeRepository carrierVolumeRepository;
	
	@Autowired
	CarrierRepository carrierRepository;
	
	@Autowired
	TimeTableRepository timeTableRepository;
	
	@Autowired
	VolumeFlowRepository volumeFlowRepository;
	
	@Autowired
	LocationRepository locationRepository;
	
	@Autowired
	MatrixRepository matrixRepository;
	
	@Autowired
	TransportTemplateRepository transportTemplateRepository;
	
	@Autowired
	TimeTableTemplateRepository timeTableTemplateRepository;
	
	@Autowired
	GnssRepository gnssRepository;
	
	public void transportJsonParsing() {
		this.eventHubOperation.setStartPosition(StartPosition.EARLIEST);
		this.eventHubOperation
			.setCheckpointConfig(CheckpointConfig.builder().checkpointMode(CheckpointMode.MANUAL).build());
		//System.out.println("setting check points");
		this.eventHubOperation.getCheckpointConfig();
		this.eventHubOperation.subscribe(transportEventHubName, consumerName, t -> {
			try {
				transportMessageReceiver(t);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}, JsonNode.class);
	     System.out.println("After subscription");
		
	}
	
    private void transportMessageReceiver(Message<?> message) throws Exception  {
	
		//System.out.println("message " + message.getPayload().toString());
	
		getTransportJsonData(message.getPayload());
		Checkpointer checkpointer = message.getHeaders().get(AzureHeaders.CHECKPOINTER, Checkpointer.class);
		checkpointer.success()
				.doOnSuccess(s -> System.out.println(String.format("Message successfully checkpointed")))
			.doOnError(System.out::println).subscribe();


	}	
    @Async
    public void getTransportJsonData(Object message) throws Exception {
	System.out.println("parsing data ");
	XmlMapper xmlMapper = new XmlMapper();
	ObjectMapper mapper = new ObjectMapper();
	String txt = "";
	TransportMessage transportMessage = null;
	try {
	txt = xmlMapper.writeValueAsString(message);
	System.out.println(txt);
	transportMessage = xmlMapper.readValue(txt, TransportMessage.class);
	String validateJsonData = mapper.writeValueAsString(transportMessage);
	}
	catch (Exception ex) {
		throw new Exception(ex.getMessage());
	}
	Payload payload=transportMessage.getPayload();
	Transport transport=new Transport();
	

	transport.setTs(BigInteger.valueOf(payload.getTs()));
	transport.setTransportId(payload.getId());
	transport.setTransportTemplate(payload.getTransportMall());
	transport.setTransportType(payload.getTransportTyp());
	transport.setTransportNo(payload.getTransportNr());
	transport.setTransportDescription(payload.getTransportBeskrivning());
	transport.setProdDay(payload.getProdDygn());
	transport.setOwner(payload.getAgare());
	transport.setHaulageContracter(payload.getAkeri());
	transport.setSet(payload.getInstalld());
	transport.setCauseOfExtraTransport(payload.getOrsakExtraTransport());
	
	
	transportRepository.save(transport);

   }

	public void carrierVolume() {
		this.eventHubOperation.setStartPosition(StartPosition.EARLIEST);
		this.eventHubOperation
			.setCheckpointConfig(CheckpointConfig.builder().checkpointMode(CheckpointMode.MANUAL).build());
		System.out.println("setting check points");
		this.eventHubOperation.getCheckpointConfig();
		this.eventHubOperation.subscribe(carrierVolumeEventHubName, consumerName, t -> {
			try {
				carrierVolumeMessageReceiver(t);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}, JsonNode.class);
	     System.out.println("After subscription");
		
	}
	
	private void carrierVolumeMessageReceiver(Message<?> message) throws Exception  {
		
		//System.out.println("message " + message.getPayload().toString());
	
		getCarrierVolumeJsonData(message.getPayload());
		Checkpointer checkpointer = message.getHeaders().get(AzureHeaders.CHECKPOINTER, Checkpointer.class);
		checkpointer.success()
				.doOnSuccess(s -> System.out.println(String.format("Message successfully checkpointed")))
			.doOnError(System.out::println).subscribe();

	}	
	
	@Async
	public void getCarrierVolumeJsonData(Object message) throws Exception {
		XmlMapper xmlMapper = new XmlMapper();
		ObjectMapper mapper = new ObjectMapper();
		String txt = "";
		CarrierVolumeMessage carrierVolumeMessage = null;
		try {
			txt = xmlMapper.writeValueAsString(message);
			System.out.println(txt);
			carrierVolumeMessage = xmlMapper.readValue(txt, CarrierVolumeMessage.class);
			String validateJsonData = mapper.writeValueAsString(carrierVolumeMessage);
			}
			catch (Exception ex) {
				throw new Exception(ex.getMessage());
			}
		
				CarrierVolumePayload carrierVolumePayload= carrierVolumeMessage.getPayload();
				CarrierVolume carrierVolume= new CarrierVolume();	
				
				carrierVolume.setTs(BigInteger.valueOf(carrierVolumePayload.getTs()));
				carrierVolume.setVolumeId(carrierVolumePayload.getVolymId());
				carrierVolume.setHighLoad(carrierVolumePayload.getHighLoad());
				carrierVolume.setPalletVolume(carrierVolumePayload.getPalletVolume());
				carrierVolume.setAstaVolume(carrierVolumePayload.getAstaVolume());
				carrierVolume.setAstaCount(carrierVolumePayload.getAstaCount());
				carrierVolume.setAstaWeight(carrierVolumePayload.getAstaWeight());
				carrierVolume.setAstaMinLoadDatetime(carrierVolumePayload.getAstaMinLoadDatetime());
				carrierVolume.setAstaMaxLoadDatetime(carrierVolumePayload.getAstaMaxLoadDatetime());
				carrierVolume.setParcelsLooseload(carrierVolumePayload.getParcelsLooseload());
				carrierVolume.setParcelsContainer(carrierVolumePayload.getParcelsContainer());
				carrierVolume.setParcelsCardboard(carrierVolumePayload.getParcelsCardboard());
				carrierVolume.setParcelsPallet(carrierVolumePayload.getParcelsPallet());
				carrierVolume.setParcelsLc(carrierVolumePayload.getParcelsLc());
				carrierVolume.setParcelsLcage(carrierVolumePayload.getParcelsLcage());
				carrierVolume.setParcelsScage(carrierVolumePayload.getParcelsScage());
				carrierVolume.setEmbContainer(carrierVolumePayload.getEmbContainer());
				carrierVolume.setEmbCardboard(carrierVolumePayload.getEmbCardboard());
				carrierVolume.setEmbPallet(carrierVolumePayload.getEmbPallet());
				carrierVolume.setEmbLc(carrierVolumePayload.getEmbLc());
				carrierVolume.setEmbLcage(carrierVolumePayload.getEmbLcage());
				carrierVolume.setPallettContainer(carrierVolumePayload.getPallettContainer());
				carrierVolume.setPallettPallett(carrierVolumePayload.getPalletPallet());
				carrierVolume.setExpressContainer(carrierVolumePayload.getExpressContainer());
				carrierVolume.setExpressPallet(carrierVolumePayload.getExpressPallet());
				carrierVolume.setDpdContainer(carrierVolumePayload.getDpdContainer());
				carrierVolume.setDpdCardboard(carrierVolumePayload.getDpdCardboard());
				carrierVolume.setDpdPallet(carrierVolumePayload.getDpdPallet());
				carrierVolume.setEmptyContainer(carrierVolumePayload.getEmptyContainer());
				carrierVolume.setEmptyPallet(carrierVolumePayload.getEmptyPallet());
				carrierVolume.setEmptyLc(carrierVolumePayload.getEmptyLc());
				carrierVolume.setEmptyLcage(carrierVolumePayload.getEmptyLcage());
				carrierVolume.setEmptyScage(carrierVolumePayload.getEmptyScage());
				carrierVolume.setEmptyBc(carrierVolumePayload.getEmptyBc());
				carrierVolume.setInnightPallet(carrierVolumePayload.getInnightPallet());
				carrierVolume.setInnightLcage(carrierVolumePayload.getInnightLcage());
				carrierVolume.setInnightScage(carrierVolumePayload.getInnightScage());
				carrierVolume.setLetteraPallet(carrierVolumePayload.getLetteraPallet());
				carrierVolume.setLetteraLc(carrierVolumePayload.getLetterbLc());
				carrierVolume.setLetteraBc(carrierVolumePayload.getLetteraBc());
				carrierVolume.setLetteraPallet(carrierVolumePayload.getLetteraPallet());
				carrierVolume.setLetterbLc(carrierVolumePayload.getLetterbLc());
				carrierVolume.setLetterbBc(carrierVolumePayload.getLetterbBc());
				carrierVolume.setFillratio(carrierVolumePayload.getFyllnadsgrad());
			
				carrierVolumeRepository.save(carrierVolume);
		
		
		
	}

	public void carrierEventHubData() {
		
		this.eventHubOperation.setStartPosition(StartPosition.EARLIEST);
		this.eventHubOperation
			.setCheckpointConfig(CheckpointConfig.builder().checkpointMode(CheckpointMode.MANUAL).build());
		System.out.println("setting check points");
		this.eventHubOperation.getCheckpointConfig();
		this.eventHubOperation.subscribe(carrierEventHubName, consumerName, t -> {
			try {
				carrierMessageReceiver(t);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}, JsonNode.class);
	     System.out.println("After subscription");
	}
	
	
	 private void carrierMessageReceiver(Message<?> message) throws Exception  {
			
			//System.out.println("message " + message.getPayload().toString());
		
			carrierJsonData(message.getPayload());
			Checkpointer checkpointer = message.getHeaders().get(AzureHeaders.CHECKPOINTER, Checkpointer.class);
			checkpointer.success()
					.doOnSuccess(s -> System.out.println(String.format("Message successfully checkpointed")))
				.doOnError(System.out::println).subscribe();


		}	
	    @Async
	    public void carrierJsonData(Object message) throws Exception {
			XmlMapper xmlMapper = new XmlMapper();
				ObjectMapper mapper = new ObjectMapper();
				String txt = "";
				CarrierMessage carrierMessage= null;
				try {
					txt = xmlMapper.writeValueAsString(message);
					System.out.println(txt);
					carrierMessage = xmlMapper.readValue(txt, CarrierMessage.class);
					String validateJsonData = mapper.writeValueAsString(carrierMessage);
					}
					catch (Exception ex) {
						throw new Exception(ex.getMessage());
					}
				
				CarrierMessagePayload carrierMessagePayload=carrierMessage.getPayload();
				Carrier carrier= new Carrier();
				
				carrier.setCarrierId(carrierMessagePayload.getId());
				carrier.setAssetId(carrierMessagePayload.getLbId());
				carrier.setAssetType(carrierMessagePayload.getLastbtyp());
				carrier.setAssetRegno(carrierMessagePayload.getRegnr());
				carrier.setGpsUnit(carrierMessagePayload.getGpsenhet());
				carrier.setGpsUnitInactive(carrierMessagePayload.getGpsEnhetInactive());
				carrier.setCountry(carrierMessagePayload.getLand());
				carrier.setHireVehicle(carrierMessagePayload.getHyrfordon());
				carrier.setTempSensor(carrierMessagePayload.getTempsenor());
				carrier.setYear(carrierMessagePayload.getArsmodell());
				carrier.setSupplier(carrierMessagePayload.getLeverantor());
				carrier.setLastInspection(carrierMessagePayload.getBesiktigad());
				carrier.setStatus(carrierMessagePayload.getStatus());
				carrier.setDamageExplanation(carrierMessagePayload.getSkadeForklaring());
				
			    carrier.setLoadable(carrierMessagePayload.getLoadable());
			    carrier.setP100Pallets(carrierMessagePayload.getP100Pallet());
				carrier.setP100Parcels(carrierMessagePayload.getP100Parcel());
			    carrier.setAllowedVolume(carrierMessagePayload.getAllowedVolume());
			    carrier.setVolume(carrierMessagePayload.getVolume());
			    
				carrierRepository.save(carrier);
				
				
			}

		public void timeTableEventHubData() {
			this.eventHubOperation.setStartPosition(StartPosition.EARLIEST);
			this.eventHubOperation
				.setCheckpointConfig(CheckpointConfig.builder().checkpointMode(CheckpointMode.MANUAL).build());
			System.out.println("setting check points");
			this.eventHubOperation.getCheckpointConfig();
			this.eventHubOperation.subscribe(timeTableEventHubName, consumerName, t -> {
				try {
					timeTableMessageReceiver(t);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}, JsonNode.class);
		     System.out.println("After subscription");
		}
		
		
		 private void timeTableMessageReceiver(Message<?> message) throws Exception  {
				
				//System.out.println("message " + message.getPayload().toString());
			
				timeTableJsonData(message.getPayload());
				Checkpointer checkpointer = message.getHeaders().get(AzureHeaders.CHECKPOINTER, Checkpointer.class);
				checkpointer.success()
						.doOnSuccess(s -> System.out.println(String.format("Message successfully checkpointed")))
					.doOnError(System.out::println).subscribe();


			}	
		    @Async
		    public void timeTableJsonData(Object message) throws Exception {
			    XmlMapper xmlMapper = new XmlMapper();
				ObjectMapper mapper = new ObjectMapper();
				String txt = "";
				TimeTableMessage timeTableMessage= null;
				try {
					txt = xmlMapper.writeValueAsString(message);
					System.out.println(txt);
					timeTableMessage = xmlMapper.readValue(txt, TimeTableMessage.class);
					String validateJsonData = mapper.writeValueAsString(timeTableMessage);
					}
					catch (Exception ex) {
						throw new Exception(ex.getMessage());
					}
				
				TimeTablePayload timetablePayload= timeTableMessage.getPayload();
		        TimeTable timetalbe= new TimeTable();
		
		       
		       
		        timetalbe.setTs(BigInteger.valueOf(timetablePayload.getTs()));
		        timetalbe.setTimetableId(timetablePayload.getId());
		        timetalbe.setTransport(timetablePayload.getTransport());
		        timetalbe.setJourneyLeg(timetablePayload.getDelstracka());
		        timetalbe.setDepartureLocationId(timetablePayload.getAvgPlats());
		        timetalbe.setDepartureLocationName(timetablePayload.getAvgAng());
		        timetalbe.setDepartureDate(timetablePayload.getAvgDatum());
		        timetalbe.setDepartureTime(timetablePayload.getAvgTid());
		        timetalbe.setActualDepartureTime(timetablePayload.getVerklAvgTid());
		        timetalbe.setDepartureLocation(timetablePayload.getAvgAnm());
		        timetalbe.setArrivalLocationId(timetablePayload.getAnkPlats());
		        timetalbe.setArrivalLocationName(timetablePayload.getAnkAng());
		        timetalbe.setArrivalDate(timetablePayload.getAnkDatum());
		        timetalbe.setArrivalTime(timetablePayload.getAnkTid());
		        timetalbe.setActualArrivalTime(timetablePayload.getVerklAnkTid());
		        timetalbe.setArrivalLocation(timetablePayload.getAnkAnm());
		       
		        timetalbe.setDistance(timetablePayload.getStracka());
		        timetalbe.setDepartureSet(timetablePayload.getAvgInstalld());
		        timetalbe.setArrivalSet(timetablePayload.getAnkInstalld());
		       
		        timetalbe.setAssertTypeId1(timetablePayload.getLastbDef1());
		        timetalbe.setAssertType1(timetablePayload.getLastb1());
		        timetalbe.setAssetId1(timetablePayload.getLbid1());
		        
		        timetalbe.setAssetRegno1(timetablePayload.getRegnr1());
		        timetalbe.setCapacity1(timetablePayload.getVolymid1());
		        timetalbe.setUtilizationRate1(timetablePayload.getFyllnadsgrad1());
		       
		        timetalbe.setAssetTypeId2(timetablePayload.getLastbDef2());
		        timetalbe.setAssetType2(timetablePayload.getLastb2());
		        timetalbe.setAssetId2(timetablePayload.getLbid2());
		        timetalbe.setAssetRegno2(timetablePayload.getRegnr2());
		        timetalbe.setCapacity2(timetablePayload.getVolymid2());
		        timetalbe.setUtilizationRate2(timetablePayload.getFyllnadsgrad2());
		       
		        timetalbe.setAssetTypeId3(timetablePayload.getLastbDef3());
		        timetalbe.setAssetType3(timetablePayload.getLastb3());
		        timetalbe.setAssetId3(timetablePayload.getLbid3());
		        timetalbe.setAssetRegno3(timetablePayload.getRegnr3());
		        timetalbe.setCapacity3(timetablePayload.getVolymid3());
		        timetalbe.setUtilizationRate3(timetablePayload.getFyllnadsgrad3());
		       
		        timetalbe.setAssetTypeId4(timetablePayload.getLastbDef4());
		        timetalbe.setAssetType4(timetablePayload.getLastb4());
		        timetalbe.setAssetId4(timetablePayload.getLbid4());
		        timetalbe.setAssetRegno4(timetablePayload.getRegnr4());
		        timetalbe.setCapacity4(timetablePayload.getVolymid4());
		        timetalbe.setUtilizationRate4(timetablePayload.getFyllnadsgrad4());
		       
		       
		        timeTableRepository.save(timetalbe);
				
				
			}
		    
		    

 public CarrierMessagePayload carrier(JsonNode inputJson) throws Exception {
	XmlMapper xmlMapper = new XmlMapper();
		ObjectMapper mapper = new ObjectMapper();
		String txt = "";
		CarrierMessage carrierMessage= null;
		try {
			txt = xmlMapper.writeValueAsString(inputJson);
			System.out.println(txt);
			carrierMessage = xmlMapper.readValue(txt, CarrierMessage.class);
			String validateJsonData = mapper.writeValueAsString(carrierMessage);
			}
			catch (Exception ex) {
				throw new Exception(ex.getMessage());
			}
		
		CarrierMessagePayload carrierMessagePayload=carrierMessage.getPayload();
		Carrier carrier= new Carrier();
		carrier.setCarrierId(String.valueOf(carrierMessagePayload.getId()));
		carrier.setAssetId(String.valueOf(carrierMessagePayload.getLbId()));
		carrier.setAssetType(carrierMessagePayload.getLastbtyp());
		carrier.setAssetRegno(carrierMessagePayload.getRegnr());
		carrier.setGpsUnit(String.valueOf(carrierMessagePayload.getGpsenhet()));
		carrier.setGpsUnitInactive(carrierMessagePayload.getGpsEnhetInactive());
		carrier.setCountry(carrierMessagePayload.getLand());
		carrier.setHireVehicle(String.valueOf(carrierMessagePayload.getHyrfordon()));
		carrier.setTempSensor(String.valueOf(carrierMessagePayload.getTempsenor()));
		carrier.setYear(String.valueOf(carrierMessagePayload.getArsmodell()));
		carrier.setSupplier(carrierMessagePayload.getLeverantor());
		carrier.setLastInspection(String.valueOf(carrierMessagePayload.getBesiktigad()));
		carrier.setStatus(carrierMessagePayload.getStatus());
		carrier.setDamageExplanation(carrierMessagePayload.getSkadeForklaring());
		
	    carrier.setLoadable(String.valueOf(carrierMessagePayload.getLoadable()));
	    carrier.setP100Pallets(carrierMessagePayload.getP100Pallet());
		carrier.setP100Parcels(carrierMessagePayload.getP100Parcel());
	    carrier.setAllowedVolume(String.valueOf(carrierMessagePayload.getAllowedVolume()));
	    carrier.setVolume(String.valueOf(carrierMessagePayload.getVolume()));
	    
		carrierRepository.save(carrier);
		return carrierMessagePayload;
		
	}



       

public LocationMessagePayload locationMessage(JsonNode inputJson) throws Exception {
	XmlMapper xmlMapper = new XmlMapper();
	ObjectMapper mapper = new ObjectMapper();
	String txt = "";
	
	LocationMessage locationMessage= null;
	try {
	txt = xmlMapper.writeValueAsString(inputJson);
	System.out.println(txt);
	locationMessage = xmlMapper.readValue(txt, LocationMessage.class);
	String validateJsonData = mapper.writeValueAsString(locationMessage);
	}
	catch (Exception ex) {
		throw new Exception(ex.getMessage());
	}
	
	LocationMessagePayload locationMessagePayload=locationMessage.getPayload();
	Location location= new Location();
	location.setId(String.valueOf(locationMessagePayload.getId()));
	location.setCountry(locationMessagePayload.getLand());
	location.setRegion(locationMessagePayload.getProdomr());
	location.setPlaceName(locationMessagePayload.getOrtnamn());
	location.setCard(locationMessagePayload.getPostort());
	location.setLocationType(locationMessagePayload.getAngtyp());
	location.setLocationName(locationMessagePayload.getAngoring());
	
	location.setStreet(locationMessagePayload.getGata());
	location.setStreetNo(String.valueOf(locationMessagePayload.getGataNr()));
	location.setStreetNoPrefix(locationMessagePayload.getGataNrPrefix());
	location.setPostCode(String.valueOf(locationMessagePayload.getPostnr()));
	location.setLat(locationMessagePayload.getLat());
	location.setLng(locationMessagePayload.getLng());
	 
  
	locationRepository.save(location);
	return locationMessagePayload;

}
public MatrixMessagePayload sendmatrix( JsonNode inputJson) throws Exception {

	XmlMapper xmlMapper = new XmlMapper();
	ObjectMapper mapper = new ObjectMapper();
	String txt = "";
	MatrixMessage matrixMessage=null;
	try {
		txt = xmlMapper.writeValueAsString(inputJson);
		matrixMessage = xmlMapper.readValue(txt, MatrixMessage.class);
		String validateJsonData = mapper.writeValueAsString(matrixMessage);
		}
		catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	MatrixMessagePayload matrixMessagePayload=matrixMessage.getPayload();
	Matrix matrix= new Matrix();

	matrix.setOriginTerminalId(String.valueOf(matrixMessagePayload.getStartP()));
	matrix.setOriginTerminalName(matrixMessagePayload.getStartName());
	matrix.setDestinationTerminalId(String.valueOf(matrixMessagePayload.getStopP()));
	matrix.setDestinationTerminalName(matrixMessagePayload.getStopName());
	matrix.setDistance(matrixMessagePayload.getKm());
	matrix.setHours(matrixMessagePayload.getHours());
	
	matrix.setMinutes(matrixMessagePayload.getMinutes());

	matrix.setSpeed((int) matrixMessagePayload.getSpeedLimit());
	

	matrixRepository.save(matrix);
	return matrixMessagePayload;
	

}

public TransportTemplatePayload sendtransporttemplate( JsonNode inputJson) throws Exception {
	XmlMapper xmlMapper = new XmlMapper();
	ObjectMapper mapper = new ObjectMapper();
	String txt = "";
	TransportTemplateMessage transportTemplateMessage=null;
	try {
		txt = xmlMapper.writeValueAsString(inputJson);
		transportTemplateMessage = xmlMapper.readValue(txt, TransportTemplateMessage.class);
		String validateJsonData = mapper.writeValueAsString(transportTemplateMessage);
		}
		catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	TransportTemplatePayload transportTemplatePayload=transportTemplateMessage.getPayload();
	TransportTemplate transportTemplate=new TransportTemplate();
	transportTemplate.setTransportId(String.valueOf(transportTemplatePayload.getId()));
	
	transportTemplate.setTransportType(transportTemplatePayload.getTransporttyp());
	transportTemplate.setTransportNumber(String.valueOf(transportTemplatePayload.getTransportnr()));
	
	transportTemplate.setTransportDescription(transportTemplatePayload.getTransportBeskrivning());
	//transportTemplate.setStartDate(Date.valueOf(transportTemplatePayload.getStartdatum()));
	transportTemplate.setStartDate(transportTemplatePayload.getStartdatum());
	
	transportTemplate.setEndDate(transportTemplatePayload.getSlutdatum());

	transportTemplate.setOwner(transportTemplatePayload.getAgare());
	
	transportTemplateRepository.save(transportTemplate);
	return transportTemplatePayload;
}

public GnssMessagePayload sendgnss( JsonNode inputJson) throws Exception {
	
	XmlMapper xmlMapper = new XmlMapper();
	ObjectMapper mapper = new ObjectMapper();
	String txt = "";
	GnssDataMessage gnssDataMessage=null;
	try {
		txt = xmlMapper.writeValueAsString(inputJson);
		gnssDataMessage = xmlMapper.readValue(txt, GnssDataMessage.class);
		String validateJsonData = mapper.writeValueAsString(gnssDataMessage);
		}
		catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	GnssMessagePayload gnssMessagePayload=gnssDataMessage.getPayload();
	GnssData gnssData=new GnssData();
	
    gnssData.setIotDeviceID(gnssMessagePayload.getIotDeviceID());
	/*OffsetDateTime odt = OffsetDateTime.parse( gnssMessagePayload.getEventTime() );
	System.out.println(odt);
	gnssData.setEventTime(odt.toZonedDateTime());
	System.out.println(odt.toZonedDateTime());
	System.out.println((DateUtility.StringToTimeStamp(gnssMessagePayload.getEventTime())));
	gnssData.setServiceProviderServerTime(DateUtility.stringToJsonDate(gnssMessagePayload.getServiceProviderServerTime()));
	OffsetDateTime odt1 = OffsetDateTime.parse( (gnssMessagePayload.getServiceProviderServerTime()) );
	System.out.println(odt1);
	gnssData.setServiceProviderServerTime(OffsetDateTime.parse(gnssMessagePayload.getServiceProviderServerTime()).toZonedDateTime());
	System.out.println(OffsetDateTime.parse(gnssMessagePayload.getServiceProviderServerTime()).toZonedDateTime());
    
    */
    gnssData.setEventTime(gnssMessagePayload.getEventTime());
    
	gnssData.setServiceProviderServerTime(gnssMessagePayload.getServiceProviderServerTime());
	
	gnssData.setEventType((gnssMessagePayload.getEventType()));
	gnssData.setServiceProviderDeviceID(gnssMessagePayload.getServiceProviderDeviceID());
	//gnssData.setIotServerTime(DateUtility.stringToJsonDate(gnssMessagePayload.getIotServerTime()));
	
	gnssData.setIotServerTime(gnssMessagePayload.getIotServerTime());
	gnssData.setLongitude(String.valueOf(gnssMessagePayload.getLongitude()));
	gnssData.setLatitude(String.valueOf(gnssMessagePayload.getLatitude()));
	gnssData.setSpeed(String.valueOf(gnssMessagePayload.getSpeed()));
	gnssData.setHeading(String.valueOf(gnssMessagePayload.getHeading()));
	gnssData.setOdometer(String.valueOf(gnssMessagePayload.getOdometer()));
    gnssData.setIgnition(String.valueOf(gnssMessagePayload.getIgnition()));
    gnssData.setServiceProvider(gnssMessagePayload.getServiceProvider());
	gnssData.setDomain(gnssMessagePayload.getDomain());
	gnssData.setGpsQuality(gnssMessagePayload.getGPSQuality());
	gnssData.setAltitude(gnssMessagePayload.getAltitude());
	gnssData.setBatteryVoltage(gnssMessagePayload.getBatteryVoltage());
	 gnssData.setTempSensor(gnssMessagePayload.getTempSensor());
	 gnssData.setDoorSensor(gnssMessagePayload.getDoorSensor());
	 gnssData.setShockSensor(gnssMessagePayload.getShockSensor());
	 gnssData.setEbsBrakeLiningOk(gnssMessagePayload.getEbsBrakeLining_ok());
	 gnssData.setEbsCoupled(gnssMessagePayload.getEbsCoupled());
	gnssData.setEbsIgnition(gnssMessagePayload.getEbsIgnition());
	gnssData.setEbsLoad(gnssMessagePayload.getEbsLoad());
	gnssData.setExternalPower(gnssMessagePayload.getExternalPower());
//	gnssData.setLastVehicleMovementTimestamp(DateUtility.stringToJsonDate(gnssMessagePayload.getLastVehicleMovementTimestamp()));
	gnssData.setLastVehicleMovementTimestamp(gnssMessagePayload.getLastVehicleMovementTimestamp());
	  
	gnssRepository.save(gnssData);
	return gnssMessagePayload;
	
}

public void volumeFlowMessage(JsonNode inputJson) throws Exception {
	XmlMapper xmlMapper = new XmlMapper();
	ObjectMapper mapper = new ObjectMapper();
	String txt = "";
	
	VolumeFlowMessage volumeFlowMessage= null;
	try {
	txt = xmlMapper.writeValueAsString(inputJson);
	System.out.println(txt);
	volumeFlowMessage = xmlMapper.readValue(txt, VolumeFlowMessage.class);
	String validateJsonData = mapper.writeValueAsString(volumeFlowMessage);
	}
	catch (Exception ex) {
		throw new Exception(ex.getMessage());
	}
	
	VolumeFlowMessagePayload volumeFlowMessagePayload=volumeFlowMessage.getPayload();
	VolumeFlow volumeFlow= new VolumeFlow();
	VolumeFlowPk  volumeFlowPk=new VolumeFlowPk();
	
	volumeFlowPk.setOriginTerminalId(volumeFlowMessagePayload.getOrigin());
	volumeFlowPk.setDestinationTerminalId(volumeFlowMessagePayload.getDestination());
	volumeFlowPk.setDate(DateUtility.stringToJsonDate(volumeFlowMessagePayload.getDates()));
	volumeFlowPk.setTime(DateUtility.stringToLocalTime(String.valueOf(volumeFlowMessagePayload.getTimes())));
	volumeFlow.setVolumeFlowPk(volumeFlowPk);
	volumeFlow.setNoOfParcelQuantity(Integer.valueOf(String.valueOf(volumeFlowMessagePayload.getCount())));
	volumeFlow.setParcelVolume(Double.valueOf(String.valueOf(volumeFlowMessagePayload.getVolume())));
	volumeFlow.setCreatedBy("Admin");
	volumeFlow.setCreatedTs(new Date());
	volumeFlow.setUpdatedBy("User");
        volumeFlow.setUpdatedTs(new Date());
    
	volumeFlowRepository.save(volumeFlow);
	

}


public TimeTableTemplatePayload timeTableTemplate( JsonNode inputJson) throws Exception {
    XmlMapper xmlMapper = new XmlMapper();
	ObjectMapper mapper = new ObjectMapper();
	String txt = "";
	TimeTableTemplateMessage timeTableTemplateMessage= null;
	try {
		txt = xmlMapper.writeValueAsString(inputJson);
		System.out.println(txt);
		timeTableTemplateMessage = xmlMapper.readValue(txt, TimeTableTemplateMessage.class);
		String validateJsonData = mapper.writeValueAsString(timeTableTemplateMessage);
		}
		catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	
	TimeTableTemplatePayload timetableTemplatePayload= timeTableTemplateMessage.getPayload();
    TimeTableTemplate timetalbeTemplate= new TimeTableTemplate();
    
   
   
    timetalbeTemplate.setTimetableId(String.valueOf(timetableTemplatePayload.getId()));
    timetalbeTemplate.setTransportId(String.valueOf(timetableTemplatePayload.getTransport()));
    timetalbeTemplate.setJourneyLeg(String.valueOf(timetableTemplatePayload.getDelstracka()));
    timetalbeTemplate.setDepartureLocationId(String.valueOf(timetableTemplatePayload.getAvgPlats()));
    timetalbeTemplate.setDepartureLocationName(String.valueOf(timetableTemplatePayload.getAvgAng()));
    timetalbeTemplate.setDepartureStartDate(timetableTemplatePayload.getAvgStartdatum());
    timetalbeTemplate.setDepartureEndDate(timetableTemplatePayload.getAvgSlutdatum());
    //timetalbeTemplate.setDepartureStartDate((timetableTemplatePayload.getAvgStartdatum()));
    //timetalbeTemplate.setDepartureEndDate((timetableTemplatePayload.getAvgSlutdatum()));
    timetalbeTemplate.setDepartureTime(String.valueOf(timetableTemplatePayload.getAvgTid()));
   
    timetalbeTemplate.setArrivalLocationId(String.valueOf(timetableTemplatePayload.getAnkPlats()));
    timetalbeTemplate.setArrivalLocationName(String.valueOf(timetableTemplatePayload.getAnkAng()));
    timetalbeTemplate.setArrivalStartDate(timetableTemplatePayload.getAnkStartdatum());
    timetalbeTemplate.setArrivalEndDate(timetableTemplatePayload.getAnkSlutdatum());
   // timetalbeTemplate.setArrivalStartDate((timetableTemplatePayload.getAnkStartdatum()));
   // timetalbeTemplate.setArrivalEndDate((timetableTemplatePayload.getAnkSlutdatum()));
    timetalbeTemplate.setArrivalTime(String.valueOf(timetableTemplatePayload.getAnkTid()));
    timetalbeTemplate.setTransportType(String.valueOf(timetableTemplatePayload.getTransporttyp()));
   
    timeTableTemplateRepository.save(timetalbeTemplate);
	return timetableTemplatePayload;
	
}




			
		
}
