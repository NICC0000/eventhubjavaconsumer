package com.tcs.eventhub.repositories;

import org.springframework.data.repository.CrudRepository;

import com.tcs.eventhub.model.TimeTableTemplate;

public interface TimeTableTemplateRepository extends CrudRepository<TimeTableTemplate,String>{

}
