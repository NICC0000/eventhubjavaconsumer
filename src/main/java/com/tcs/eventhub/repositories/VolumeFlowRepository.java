package com.tcs.eventhub.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.eventhub.model.VolumeFlow;
import com.tcs.eventhub.model.VolumeFlowPk;

@Repository
public interface VolumeFlowRepository extends CrudRepository<VolumeFlow, VolumeFlowPk> {

}
