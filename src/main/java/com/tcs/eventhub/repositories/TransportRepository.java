package com.tcs.eventhub.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.eventhub.model.Transport;


@Repository
public interface TransportRepository extends CrudRepository<Transport, String>  {

}
