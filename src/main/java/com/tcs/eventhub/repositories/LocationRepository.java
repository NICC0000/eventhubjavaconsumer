package com.tcs.eventhub.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.eventhub.model.Location;
@Repository
public interface LocationRepository extends CrudRepository<Location, String> {

}
