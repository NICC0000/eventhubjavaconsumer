package com.tcs.eventhub.repositories;

import java.math.BigInteger;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.eventhub.model.CarrierVolume;

@Repository
public interface CarrierVolumeRepository extends CrudRepository<CarrierVolume,BigInteger> {

}
