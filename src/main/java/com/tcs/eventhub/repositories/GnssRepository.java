package com.tcs.eventhub.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.eventhub.model.GnssData;
@Repository
public interface GnssRepository  extends CrudRepository<GnssData, String> {

}
