package com.tcs.eventhub.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.eventhub.model.TimeTable;


@Repository
public interface TimeTableRepository extends CrudRepository<TimeTable,String> {

}
