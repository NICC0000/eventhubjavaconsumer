package com.tcs.eventhub.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.eventhub.model.TransportTemplate;
@Repository
public interface TransportTemplateRepository extends CrudRepository<TransportTemplate, String> {

}
