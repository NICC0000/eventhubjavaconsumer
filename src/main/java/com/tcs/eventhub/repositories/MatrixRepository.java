package com.tcs.eventhub.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.eventhub.model.Matrix;
@Repository
public interface MatrixRepository extends CrudRepository<Matrix, String> {

}
