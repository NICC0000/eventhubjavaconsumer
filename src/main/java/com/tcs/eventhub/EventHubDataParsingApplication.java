package com.tcs.eventhub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class EventHubDataParsingApplication {
	public static final Logger LOGGER = LoggerFactory.getLogger(EventHubDataParsingApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(EventHubDataParsingApplication.class, args);
	}

}
